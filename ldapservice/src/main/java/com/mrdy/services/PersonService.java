/**
 * 
 */
package com.mrdy.services;

import com.mrdy.model.Person;

/**
 * @author Mardy Jonathan
 *
 */

public interface PersonService {
	
	public Person auth(String username, String password);
}
