/**
 * 
 */
package com.mrdy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.LdapShaPasswordEncoder;
import org.springframework.stereotype.Service;

import com.mrdy.model.Person;
import com.mrdy.repository.PersonRepository;

/**
 * @author kentu
 *
 */
@Service
public class PersonServiceImpl implements PersonService{

	@Autowired
	private PersonRepository personRepository;
	
	@Override
	public Person auth(String username, String password) {
		return personRepository.auth(username, password);
	}

}
