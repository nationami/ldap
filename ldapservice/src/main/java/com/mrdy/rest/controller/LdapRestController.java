/**
 * 
 */
package com.mrdy.rest.controller;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mrdy.model.Person;
import com.mrdy.services.PersonService;

/**
 * @author Mardy Jonathan
 *
 */
@RestController
public class LdapRestController {
	
	@Autowired
	private PersonService personService;
	
	@Autowired
    private LdapTemplate ldapTemplate;
	
	@Value("${ldap.urls}")
	private String ldapUrls;
	
	@Value("${ldap.base}")
	private String ldapBase;
	
	@GetMapping("/")
    public String index() {
        return "Welcome to the home page!";
    }
	
	@PostMapping("/auth")
    public boolean auth(@RequestBody Person person) {
		boolean result = false;
        try {
        	Hashtable env = new Hashtable();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            env.put(Context.PROVIDER_URL, ldapUrls);
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            env.put(Context.SECURITY_PRINCIPAL, "cn="+person.getUsername()+","+ldapBase);
            env.put(Context.SECURITY_CREDENTIALS, person.getPassword());
			DirContext ctx = new InitialDirContext(env);
			Attributes attr = ctx.getAttributes("cn="+person.getUsername()+","+ldapBase);
			System.out.println(attr.get("cn").get().toString());
			result=true;
		} catch (NamingException e) {
			e.printStackTrace();
			result=false;
		}
        return result;
    }
	
	@PostMapping("/authv2")
    public Person authv2(@RequestBody Person person) {
		Person result = null;
        result = personService.auth(person.getUsername(), person.getPassword());
        return result;
    }

}
