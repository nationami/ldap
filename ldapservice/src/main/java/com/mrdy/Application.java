/**
 * 
 */
package com.mrdy;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.mrdy.model.Person;
import com.mrdy.repository.PersonRepository;

/**
 * @author Mardy Jonathan
 *
 */
@SpringBootApplication
public class Application {
	
	private static Logger log = LoggerFactory.getLogger(Application.class);

    @Autowired
    private PersonRepository personRepository;

	public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }
	
	@PostConstruct
    public void setup(){
        log.info("Spring Boot + Spring LDAP Advanced LDAP Queries Example");
        
        List<Person> names = new ArrayList<Person>();
        
        /*names = personRepository.getPersonNamesByLastName("John");
        log.info("names: " + names);*/
        
        /*names = personRepository.getPersonNamesByLastName2("Alex");
        log.info("names: " + names);*/

        /*names = personRepository.getPersonNamesByLastName3("Jahn");
        log.info("names: " + names);*/

    }
	
}
